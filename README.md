# Databáza Knižnice
Projekt vrámci predmetu BPC-BDS, Fakulta elektrotechniky a komunikačných technológií, Vysoké učení technické, Brno.

## Cieľ projektu
Vytvorenie databázového systému s možným praktickým využitím v knižniciach. Vytvorenie ER Diagramu a scrípt pre PostgreSQL a MySQL.

## Autori projektu
Miková Timea (231256), Turčanová Klára(227248)

2021
