-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema amitdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema amitdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `amitdb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `amitdb` ;

-- -----------------------------------------------------
-- Table `amitdb`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`address` (
  `address_id` INT NOT NULL AUTO_INCREMENT,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  `city` VARCHAR(45) NULL DEFAULT NULL,
  `street` VARCHAR(45) NULL DEFAULT NULL,
  `house_number` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`address_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`author`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`author` (
  `author_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NOT NULL,
  `surname` VARCHAR(20) NOT NULL,
  `mail` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`author_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`author_has_address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`author_has_address` (
  `address_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  PRIMARY KEY (`address_id`, `author_id`),
  INDEX `fk_author_has_address_address1_idx` (`address_id` ASC) VISIBLE,
  INDEX `fk_author_has_address_author1_idx` (`author_id` ASC) VISIBLE,
  CONSTRAINT `fk_author_has_address_address1`
    FOREIGN KEY (`address_id`)
    REFERENCES `amitdb`.`address` (`address_id`),
  CONSTRAINT `fk_author_has_address_author1`
    FOREIGN KEY (`author_id`)
    REFERENCES `amitdb`.`author` (`author_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`book` (
  `book_id` INT NOT NULL AUTO_INCREMENT,
  `ISBN` VARCHAR(45) NOT NULL,
  `title` VARCHAR(100) NOT NULL,
  `pages` INT NULL DEFAULT NULL,
  PRIMARY KEY (`book_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`condition`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`condition` (
  `condition_id` INT NOT NULL AUTO_INCREMENT,
  `condition` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`condition_id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`book_has_condition`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`book_has_condition` (
  `book_id` INT NOT NULL,
  `condition_id` INT NOT NULL,
  `valid_from` DATE NOT NULL,
  PRIMARY KEY (`book_id`, `condition_id`),
  INDEX `fk_book_has_condition_condition1_idx` (`condition_id` ASC) VISIBLE,
  CONSTRAINT `fk_book_has_condition_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `amitdb`.`book` (`book_id`),
  CONSTRAINT `fk_book_has_condition_condition1`
    FOREIGN KEY (`condition_id`)
    REFERENCES `amitdb`.`condition` (`condition_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`genre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`genre` (
  `genre_id` INT NOT NULL AUTO_INCREMENT,
  `genre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`genre_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`book_has_genre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`book_has_genre` (
  `book_id` INT NOT NULL,
  `genre_id` INT NOT NULL,
  PRIMARY KEY (`book_id`, `genre_id`),
  INDEX `fk_book_has_genre_genre1_idx` (`genre_id` ASC) VISIBLE,
  CONSTRAINT `fk_book_has_genre_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `amitdb`.`book` (`book_id`),
  CONSTRAINT `fk_book_has_genre_genre1`
    FOREIGN KEY (`genre_id`)
    REFERENCES `amitdb`.`genre` (`genre_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`book_written_by`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`book_written_by` (
  `author_id` INT NOT NULL,
  `book_id` INT NOT NULL,
  PRIMARY KEY (`author_id`, `book_id`),
  INDEX `fk_book_written_by_book1_idx` (`book_id` ASC) VISIBLE,
  CONSTRAINT `fk_book_written_by_author1`
    FOREIGN KEY (`author_id`)
    REFERENCES `amitdb`.`author` (`author_id`),
  CONSTRAINT `fk_book_written_by_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `amitdb`.`book` (`book_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`member_card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`member_card` (
  `membercard_id` INT NOT NULL AUTO_INCREMENT,
  `card_number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`membercard_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`member`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`member` (
  `member_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NOT NULL,
  `surname` VARCHAR(20) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `active_membership` TINYINT NULL DEFAULT NULL,
  `membercard_id` INT NOT NULL,
  PRIMARY KEY (`member_id`),
  INDEX `fk_member_member_card1_idx` (`membercard_id` ASC) VISIBLE,
  CONSTRAINT `fk_member_member_card1`
    FOREIGN KEY (`membercard_id`)
    REFERENCES `amitdb`.`member_card` (`membercard_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`member_can_borrow`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`member_can_borrow` (
  `member_id` INT NOT NULL,
  `book_id` INT NOT NULL,
  `date_from` DATE NOT NULL,
  `date_to` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`member_id`, `book_id`),
  INDEX `fk_member_can_borrow_book1_idx` (`book_id` ASC) VISIBLE,
  CONSTRAINT `fk_member_can_borrow_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `amitdb`.`book` (`book_id`),
  CONSTRAINT `fk_member_can_borrow_member1`
    FOREIGN KEY (`member_id`)
    REFERENCES `amitdb`.`member` (`member_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `amitdb`.`member_has_address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amitdb`.`member_has_address` (
  `address_id` INT NOT NULL,
  `member_id` INT NOT NULL,
  `valid_from` DATE NOT NULL,
  PRIMARY KEY (`address_id`, `member_id`),
  INDEX `fk_member_has_address_member1_idx` (`member_id` ASC) VISIBLE,
  CONSTRAINT `fk_member_has_address_address`
    FOREIGN KEY (`address_id`)
    REFERENCES `amitdb`.`address` (`address_id`),
  CONSTRAINT `fk_member_has_address_member1`
    FOREIGN KEY (`member_id`)
    REFERENCES `amitdb`.`member` (`member_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
