--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

-- Started on 2021-10-29 09:02:04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 227 (class 1259 OID 16606)
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.address (
    address_id integer NOT NULL,
    country character varying(45) NOT NULL,
    city character varying(45) NOT NULL,
    street character varying(45) NOT NULL,
    house_number character varying(45) NOT NULL
);


ALTER TABLE public.address OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 16605)
-- Name: address_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.address_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_address_id_seq OWNER TO postgres;

--
-- TOC entry 3434 (class 0 OID 0)
-- Dependencies: 226
-- Name: address_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.address_address_id_seq OWNED BY public.address.address_id;


--
-- TOC entry 222 (class 1259 OID 16580)
-- Name: author; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.author (
    author_id integer NOT NULL,
    name character varying(20) NOT NULL,
    surname character varying(20) NOT NULL,
    mail character varying(45)
);


ALTER TABLE public.author OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16579)
-- Name: author_author_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.author_author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.author_author_id_seq OWNER TO postgres;

--
-- TOC entry 3435 (class 0 OID 0)
-- Dependencies: 221
-- Name: author_author_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.author_author_id_seq OWNED BY public.author.author_id;


--
-- TOC entry 238 (class 1259 OID 16660)
-- Name: author_has_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.author_has_address (
    address_id integer NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE public.author_has_address OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 16658)
-- Name: author_has_address_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.author_has_address_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.author_has_address_address_id_seq OWNER TO postgres;

--
-- TOC entry 3436 (class 0 OID 0)
-- Dependencies: 236
-- Name: author_has_address_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.author_has_address_address_id_seq OWNED BY public.author_has_address.address_id;


--
-- TOC entry 237 (class 1259 OID 16659)
-- Name: author_has_address_author_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.author_has_address_author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.author_has_address_author_id_seq OWNER TO postgres;

--
-- TOC entry 3437 (class 0 OID 0)
-- Dependencies: 237
-- Name: author_has_address_author_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.author_has_address_author_id_seq OWNED BY public.author_has_address.author_id;


--
-- TOC entry 212 (class 1259 OID 16514)
-- Name: book; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.book (
    book_id integer NOT NULL,
    isbn character varying(45) NOT NULL,
    title character varying(20) NOT NULL,
    pages integer NOT NULL
);


ALTER TABLE public.book OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16513)
-- Name: book_book_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.book_book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_book_id_seq OWNER TO postgres;

--
-- TOC entry 3438 (class 0 OID 0)
-- Dependencies: 211
-- Name: book_book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.book_book_id_seq OWNED BY public.book.book_id;


--
-- TOC entry 220 (class 1259 OID 16562)
-- Name: book_has_condition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.book_has_condition (
    book_id integer NOT NULL,
    condition_id integer NOT NULL
);


ALTER TABLE public.book_has_condition OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16560)
-- Name: book_has_condition_book_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.book_has_condition_book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_has_condition_book_id_seq OWNER TO postgres;

--
-- TOC entry 3439 (class 0 OID 0)
-- Dependencies: 218
-- Name: book_has_condition_book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.book_has_condition_book_id_seq OWNED BY public.book_has_condition.book_id;


--
-- TOC entry 219 (class 1259 OID 16561)
-- Name: book_has_condition_condition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.book_has_condition_condition_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_has_condition_condition_id_seq OWNER TO postgres;

--
-- TOC entry 3440 (class 0 OID 0)
-- Dependencies: 219
-- Name: book_has_condition_condition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.book_has_condition_condition_id_seq OWNED BY public.book_has_condition.condition_id;


--
-- TOC entry 215 (class 1259 OID 16522)
-- Name: book_has_genre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.book_has_genre (
    book_id integer NOT NULL,
    genre_id integer NOT NULL
);


ALTER TABLE public.book_has_genre OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16520)
-- Name: book_has_genre_book_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.book_has_genre_book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_has_genre_book_id_seq OWNER TO postgres;

--
-- TOC entry 3441 (class 0 OID 0)
-- Dependencies: 213
-- Name: book_has_genre_book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.book_has_genre_book_id_seq OWNED BY public.book_has_genre.book_id;


--
-- TOC entry 214 (class 1259 OID 16521)
-- Name: book_has_genre_genre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.book_has_genre_genre_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_has_genre_genre_id_seq OWNER TO postgres;

--
-- TOC entry 3442 (class 0 OID 0)
-- Dependencies: 214
-- Name: book_has_genre_genre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.book_has_genre_genre_id_seq OWNED BY public.book_has_genre.genre_id;


--
-- TOC entry 225 (class 1259 OID 16588)
-- Name: book_written_by; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.book_written_by (
    author_id integer NOT NULL,
    book_id integer NOT NULL
);


ALTER TABLE public.book_written_by OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16586)
-- Name: book_written_by_author_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.book_written_by_author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_written_by_author_id_seq OWNER TO postgres;

--
-- TOC entry 3443 (class 0 OID 0)
-- Dependencies: 223
-- Name: book_written_by_author_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.book_written_by_author_id_seq OWNED BY public.book_written_by.author_id;


--
-- TOC entry 224 (class 1259 OID 16587)
-- Name: book_written_by_book_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.book_written_by_book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_written_by_book_id_seq OWNER TO postgres;

--
-- TOC entry 3444 (class 0 OID 0)
-- Dependencies: 224
-- Name: book_written_by_book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.book_written_by_book_id_seq OWNED BY public.book_written_by.book_id;


--
-- TOC entry 217 (class 1259 OID 16540)
-- Name: condition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.condition (
    condition_id integer NOT NULL,
    condition character varying(45)
);


ALTER TABLE public.condition OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16539)
-- Name: condition_condition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.condition_condition_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.condition_condition_id_seq OWNER TO postgres;

--
-- TOC entry 3445 (class 0 OID 0)
-- Dependencies: 216
-- Name: condition_condition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.condition_condition_id_seq OWNED BY public.condition.condition_id;


--
-- TOC entry 210 (class 1259 OID 16507)
-- Name: genre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.genre (
    genre_id integer NOT NULL,
    genre character varying(45)
);


ALTER TABLE public.genre OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16506)
-- Name: genre_genre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.genre_genre_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.genre_genre_id_seq OWNER TO postgres;

--
-- TOC entry 3446 (class 0 OID 0)
-- Dependencies: 209
-- Name: genre_genre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.genre_genre_id_seq OWNED BY public.genre.genre_id;


--
-- TOC entry 229 (class 1259 OID 16614)
-- Name: member; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.member (
    member_id integer NOT NULL,
    name character varying(20) NOT NULL,
    surname character varying(20) NOT NULL,
    mail character varying(45) NOT NULL,
    active_membership smallint NOT NULL
);


ALTER TABLE public.member OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 16622)
-- Name: member_can_borrow; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.member_can_borrow (
    member_id integer NOT NULL,
    book_id integer NOT NULL
);


ALTER TABLE public.member_can_borrow OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16621)
-- Name: member_can_borrow_book_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.member_can_borrow_book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.member_can_borrow_book_id_seq OWNER TO postgres;

--
-- TOC entry 3447 (class 0 OID 0)
-- Dependencies: 231
-- Name: member_can_borrow_book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.member_can_borrow_book_id_seq OWNED BY public.member_can_borrow.book_id;


--
-- TOC entry 230 (class 1259 OID 16620)
-- Name: member_can_borrow_member_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.member_can_borrow_member_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.member_can_borrow_member_id_seq OWNER TO postgres;

--
-- TOC entry 3448 (class 0 OID 0)
-- Dependencies: 230
-- Name: member_can_borrow_member_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.member_can_borrow_member_id_seq OWNED BY public.member_can_borrow.member_id;


--
-- TOC entry 241 (class 1259 OID 16679)
-- Name: member_card; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.member_card (
    membercard_id integer NOT NULL,
    card_number character varying(45) NOT NULL,
    member_id integer NOT NULL
);


ALTER TABLE public.member_card OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 16678)
-- Name: member_card_member_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.member_card_member_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.member_card_member_id_seq OWNER TO postgres;

--
-- TOC entry 3449 (class 0 OID 0)
-- Dependencies: 240
-- Name: member_card_member_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.member_card_member_id_seq OWNED BY public.member_card.member_id;


--
-- TOC entry 239 (class 1259 OID 16677)
-- Name: member_card_membercard_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.member_card_membercard_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.member_card_membercard_id_seq OWNER TO postgres;

--
-- TOC entry 3450 (class 0 OID 0)
-- Dependencies: 239
-- Name: member_card_membercard_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.member_card_membercard_id_seq OWNED BY public.member_card.membercard_id;


--
-- TOC entry 235 (class 1259 OID 16641)
-- Name: member_has_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.member_has_address (
    address_id integer NOT NULL,
    member_id integer NOT NULL,
    valid_from date NOT NULL
);


ALTER TABLE public.member_has_address OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 16639)
-- Name: member_has_address_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.member_has_address_address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.member_has_address_address_id_seq OWNER TO postgres;

--
-- TOC entry 3451 (class 0 OID 0)
-- Dependencies: 233
-- Name: member_has_address_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.member_has_address_address_id_seq OWNED BY public.member_has_address.address_id;


--
-- TOC entry 234 (class 1259 OID 16640)
-- Name: member_has_address_member_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.member_has_address_member_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.member_has_address_member_id_seq OWNER TO postgres;

--
-- TOC entry 3452 (class 0 OID 0)
-- Dependencies: 234
-- Name: member_has_address_member_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.member_has_address_member_id_seq OWNED BY public.member_has_address.member_id;


--
-- TOC entry 228 (class 1259 OID 16613)
-- Name: member_member_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.member_member_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.member_member_id_seq OWNER TO postgres;

--
-- TOC entry 3453 (class 0 OID 0)
-- Dependencies: 228
-- Name: member_member_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.member_member_id_seq OWNED BY public.member.member_id;


--
-- TOC entry 3241 (class 2604 OID 16609)
-- Name: address address_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address ALTER COLUMN address_id SET DEFAULT nextval('public.address_address_id_seq'::regclass);


--
-- TOC entry 3238 (class 2604 OID 16583)
-- Name: author author_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author ALTER COLUMN author_id SET DEFAULT nextval('public.author_author_id_seq'::regclass);


--
-- TOC entry 3247 (class 2604 OID 16663)
-- Name: author_has_address address_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_has_address ALTER COLUMN address_id SET DEFAULT nextval('public.author_has_address_address_id_seq'::regclass);


--
-- TOC entry 3248 (class 2604 OID 16664)
-- Name: author_has_address author_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_has_address ALTER COLUMN author_id SET DEFAULT nextval('public.author_has_address_author_id_seq'::regclass);


--
-- TOC entry 3232 (class 2604 OID 16517)
-- Name: book book_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book ALTER COLUMN book_id SET DEFAULT nextval('public.book_book_id_seq'::regclass);


--
-- TOC entry 3236 (class 2604 OID 16565)
-- Name: book_has_condition book_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_has_condition ALTER COLUMN book_id SET DEFAULT nextval('public.book_has_condition_book_id_seq'::regclass);


--
-- TOC entry 3237 (class 2604 OID 16566)
-- Name: book_has_condition condition_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_has_condition ALTER COLUMN condition_id SET DEFAULT nextval('public.book_has_condition_condition_id_seq'::regclass);


--
-- TOC entry 3233 (class 2604 OID 16525)
-- Name: book_has_genre book_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_has_genre ALTER COLUMN book_id SET DEFAULT nextval('public.book_has_genre_book_id_seq'::regclass);


--
-- TOC entry 3234 (class 2604 OID 16526)
-- Name: book_has_genre genre_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_has_genre ALTER COLUMN genre_id SET DEFAULT nextval('public.book_has_genre_genre_id_seq'::regclass);


--
-- TOC entry 3239 (class 2604 OID 16591)
-- Name: book_written_by author_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_written_by ALTER COLUMN author_id SET DEFAULT nextval('public.book_written_by_author_id_seq'::regclass);


--
-- TOC entry 3240 (class 2604 OID 16592)
-- Name: book_written_by book_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_written_by ALTER COLUMN book_id SET DEFAULT nextval('public.book_written_by_book_id_seq'::regclass);


--
-- TOC entry 3235 (class 2604 OID 16543)
-- Name: condition condition_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.condition ALTER COLUMN condition_id SET DEFAULT nextval('public.condition_condition_id_seq'::regclass);


--
-- TOC entry 3231 (class 2604 OID 16510)
-- Name: genre genre_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genre ALTER COLUMN genre_id SET DEFAULT nextval('public.genre_genre_id_seq'::regclass);


--
-- TOC entry 3242 (class 2604 OID 16617)
-- Name: member member_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member ALTER COLUMN member_id SET DEFAULT nextval('public.member_member_id_seq'::regclass);


--
-- TOC entry 3243 (class 2604 OID 16625)
-- Name: member_can_borrow member_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_can_borrow ALTER COLUMN member_id SET DEFAULT nextval('public.member_can_borrow_member_id_seq'::regclass);


--
-- TOC entry 3244 (class 2604 OID 16626)
-- Name: member_can_borrow book_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_can_borrow ALTER COLUMN book_id SET DEFAULT nextval('public.member_can_borrow_book_id_seq'::regclass);


--
-- TOC entry 3249 (class 2604 OID 16682)
-- Name: member_card membercard_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_card ALTER COLUMN membercard_id SET DEFAULT nextval('public.member_card_membercard_id_seq'::regclass);


--
-- TOC entry 3250 (class 2604 OID 16683)
-- Name: member_card member_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_card ALTER COLUMN member_id SET DEFAULT nextval('public.member_card_member_id_seq'::regclass);


--
-- TOC entry 3245 (class 2604 OID 16644)
-- Name: member_has_address address_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_has_address ALTER COLUMN address_id SET DEFAULT nextval('public.member_has_address_address_id_seq'::regclass);


--
-- TOC entry 3246 (class 2604 OID 16645)
-- Name: member_has_address member_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_has_address ALTER COLUMN member_id SET DEFAULT nextval('public.member_has_address_member_id_seq'::regclass);


--
-- TOC entry 3266 (class 2606 OID 16611)
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (address_id);


--
-- TOC entry 3274 (class 2606 OID 16666)
-- Name: author_has_address author_has_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_has_address
    ADD CONSTRAINT author_has_address_pkey PRIMARY KEY (address_id, author_id);


--
-- TOC entry 3262 (class 2606 OID 16585)
-- Name: author author_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (author_id);


--
-- TOC entry 3260 (class 2606 OID 16568)
-- Name: book_has_condition book_has_condition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_has_condition
    ADD CONSTRAINT book_has_condition_pkey PRIMARY KEY (book_id, condition_id);


--
-- TOC entry 3256 (class 2606 OID 16528)
-- Name: book_has_genre book_has_genre_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_has_genre
    ADD CONSTRAINT book_has_genre_pkey PRIMARY KEY (book_id, genre_id);


--
-- TOC entry 3254 (class 2606 OID 16519)
-- Name: book book_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (book_id);


--
-- TOC entry 3264 (class 2606 OID 16594)
-- Name: book_written_by book_written_by_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_written_by
    ADD CONSTRAINT book_written_by_pkey PRIMARY KEY (author_id, book_id);


--
-- TOC entry 3258 (class 2606 OID 16545)
-- Name: condition condition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.condition
    ADD CONSTRAINT condition_pkey PRIMARY KEY (condition_id);


--
-- TOC entry 3252 (class 2606 OID 16512)
-- Name: genre genre_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genre
    ADD CONSTRAINT genre_pkey PRIMARY KEY (genre_id);


--
-- TOC entry 3270 (class 2606 OID 16628)
-- Name: member_can_borrow member_can_borrow_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_can_borrow
    ADD CONSTRAINT member_can_borrow_pkey PRIMARY KEY (member_id, book_id);


--
-- TOC entry 3276 (class 2606 OID 16685)
-- Name: member_card member_card_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_card
    ADD CONSTRAINT member_card_pkey PRIMARY KEY (member_id);


--
-- TOC entry 3272 (class 2606 OID 16647)
-- Name: member_has_address member_has_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_has_address
    ADD CONSTRAINT member_has_address_pkey PRIMARY KEY (address_id, member_id);


--
-- TOC entry 3268 (class 2606 OID 16619)
-- Name: member member_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member
    ADD CONSTRAINT member_pkey PRIMARY KEY (member_id);


--
-- TOC entry 3287 (class 2606 OID 16667)
-- Name: author_has_address author_has_address_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_has_address
    ADD CONSTRAINT author_has_address_address_id_fkey FOREIGN KEY (address_id) REFERENCES public.address(address_id);


--
-- TOC entry 3288 (class 2606 OID 16672)
-- Name: author_has_address author_has_address_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.author_has_address
    ADD CONSTRAINT author_has_address_author_id_fkey FOREIGN KEY (author_id) REFERENCES public.author(author_id);


--
-- TOC entry 3279 (class 2606 OID 16569)
-- Name: book_has_condition book_has_condition_book_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_has_condition
    ADD CONSTRAINT book_has_condition_book_id_fkey FOREIGN KEY (book_id) REFERENCES public.book(book_id);


--
-- TOC entry 3280 (class 2606 OID 16574)
-- Name: book_has_condition book_has_condition_condition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_has_condition
    ADD CONSTRAINT book_has_condition_condition_id_fkey FOREIGN KEY (condition_id) REFERENCES public.condition(condition_id);


--
-- TOC entry 3277 (class 2606 OID 16529)
-- Name: book_has_genre book_has_genre_book_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_has_genre
    ADD CONSTRAINT book_has_genre_book_id_fkey FOREIGN KEY (book_id) REFERENCES public.book(book_id);


--
-- TOC entry 3278 (class 2606 OID 16534)
-- Name: book_has_genre book_has_genre_genre_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_has_genre
    ADD CONSTRAINT book_has_genre_genre_id_fkey FOREIGN KEY (genre_id) REFERENCES public.genre(genre_id);


--
-- TOC entry 3281 (class 2606 OID 16595)
-- Name: book_written_by book_written_by_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_written_by
    ADD CONSTRAINT book_written_by_author_id_fkey FOREIGN KEY (author_id) REFERENCES public.author(author_id);


--
-- TOC entry 3282 (class 2606 OID 16600)
-- Name: book_written_by book_written_by_book_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.book_written_by
    ADD CONSTRAINT book_written_by_book_id_fkey FOREIGN KEY (book_id) REFERENCES public.book(book_id);


--
-- TOC entry 3284 (class 2606 OID 16634)
-- Name: member_can_borrow member_can_borrow_book_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_can_borrow
    ADD CONSTRAINT member_can_borrow_book_id_fkey FOREIGN KEY (book_id) REFERENCES public.book(book_id);


--
-- TOC entry 3283 (class 2606 OID 16629)
-- Name: member_can_borrow member_can_borrow_member_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_can_borrow
    ADD CONSTRAINT member_can_borrow_member_id_fkey FOREIGN KEY (member_id) REFERENCES public.member(member_id);


--
-- TOC entry 3289 (class 2606 OID 16686)
-- Name: member_card member_card_member_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_card
    ADD CONSTRAINT member_card_member_id_fkey FOREIGN KEY (member_id) REFERENCES public.member(member_id);


--
-- TOC entry 3285 (class 2606 OID 16648)
-- Name: member_has_address member_has_address_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_has_address
    ADD CONSTRAINT member_has_address_address_id_fkey FOREIGN KEY (address_id) REFERENCES public.address(address_id);


--
-- TOC entry 3286 (class 2606 OID 16653)
-- Name: member_has_address member_has_address_member_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member_has_address
    ADD CONSTRAINT member_has_address_member_id_fkey FOREIGN KEY (member_id) REFERENCES public.member(member_id);


-- Completed on 2021-10-29 09:02:04

--
-- PostgreSQL database dump complete
--

